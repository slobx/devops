package com.slobx.www.devops;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    EditText numberET;
    Button resultButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        numberET = (EditText) findViewById(R.id.etNumber);
        resultButton = (Button) findViewById(R.id.resultButton);
    }

    private void backgroundCalculation(int number) {
        ArrayList<String> digitsArray = new ArrayList<>();

        if (number <= 0 || number > 100) {
            makeToast("Number must be between 1 and 100");
            numberET.setText("");

        } else {
            for (int i = 1; i <= number; i++) {
                digitsArray.add(i + "");
            }

            for (int i = 0; i < digitsArray.size(); i++) {
                if (Integer.parseInt(digitsArray.get(i)) % 3 == 0 && Integer.parseInt(digitsArray.get(i)) % 5 != 0) {
                    digitsArray.set(i, "Dev");
                }
                if (!digitsArray.get(i).matches("Dev")) {
                    if (Integer.parseInt(digitsArray.get(i)) % 5 == 0 && Integer.parseInt(digitsArray.get(i)) % 3 != 0) {
                        digitsArray.set(i, "Ops");
                    }
                }
                if (!digitsArray.get(i).matches("Dev") && !digitsArray.get(i).matches("Ops")) {
                    if (Integer.parseInt(digitsArray.get(i)) % 3 == 0 && Integer.parseInt(digitsArray.get(i)) % 5 == 0) {
                        digitsArray.set(i, "DevOps");
                    }
                }
            }

            Intent intent=new Intent(this, ResultActivity.class);
            intent.putStringArrayListExtra("result", digitsArray);
            startActivity(intent);
            finish();

        }
    }


    public void calculateResult(View view) {
        String inputText = numberET.getText().toString();
        if (inputText.matches("")) {
            makeToast("You must enter a number");
        } else {
            backgroundCalculation(Integer.parseInt(inputText));
        }
    }

    private void makeToast(String toastText) {
        Toast.makeText(getApplicationContext(), toastText, Toast.LENGTH_SHORT).show();
    }
}
