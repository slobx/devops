package com.slobx.www.devops;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class ResultActivity extends AppCompatActivity {

    ListView list;
    ArrayList<String> result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        list = (ListView) findViewById(R.id.list);

        result = new ArrayList<>();
        result = getIntent().getStringArrayListExtra("result");

        if (result == null) {
            result.add("Something");
            result.add("was");
            result.add("wrong");
            result.add("try again");
            result.add("please");
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, result);
        list.setAdapter(adapter);
    }

    @Override
    public void onBackPressed() {
        Intent intent= new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
